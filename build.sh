#!/usr/bin/env bash

set -e pipefail

cabal run -- rebuild && webpack -d -c && (cd site && python -m SimpleHTTPServer 8081)
