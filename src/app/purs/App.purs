module App where

import Prelude (Unit)
import Control.Monad.Eff (Eff)
import Foundation

main ∷ ∀ eff. Eff ("found" ∷ FOUND | eff) Unit
main = foundation_init
