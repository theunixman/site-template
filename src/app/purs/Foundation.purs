module Foundation where

import Prelude (Unit)
import Control.Monad.Eff (Eff)

foreign import data FOUND ∷ !
foreign import foundation_init ∷ forall eff. Eff (found ∷ FOUND | eff) Unit
