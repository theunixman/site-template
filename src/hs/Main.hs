{-# LANGUAGE OverloadedStrings #-}
module Main where

import Hakyll
import Lawless hiding (Context)
import Path

pagesSrc ∷ RelDir
pagesSrc = relDir "pages/"

layoutsSrc ∷ RelDir
layoutsSrc = relDir "layouts/"

layout ∷ RelFile → Identifier
layout l = fromFilePath ∘ toString $ layoutsSrc </> l

defaultLayout ∷ Identifier
defaultLayout = layout (relFile "default.html")

partialsSrc ∷ RelDir
partialsSrc = relDir "partials/"

wildcard ∷ RelDir → RelFile
wildcard r = r </> (relFile "*")

matchPath ∷ RelDir → Rules () → Rules ()
matchPath p = match (fromGlob ∘ toString $ wildcard p)

postCtx :: Context [Char]
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext

siteConfig ∷ Configuration
siteConfig = defaultConfiguration {
    providerDirectory = "src",
    destinationDirectory = "site"
    }

pagesRoutes ∷ Routes
pagesRoutes = gsubRoute "pages/" (const "")
    `composeRoutes` setExtension ".html"

pagesCompiler ∷ Compiler (Item [Char])
pagesCompiler =
    pandocCompiler
    >>= loadAndApplyTemplate defaultLayout postCtx

main :: IO ()
main = hakyllWith siteConfig $ do
    matchPath layoutsSrc
        $ compile templateBodyCompiler
    matchPath partialsSrc
        $ compile templateBodyCompiler
    matchPath pagesSrc $ do
        route pagesRoutes
        compile pagesCompiler
