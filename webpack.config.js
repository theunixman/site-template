"use strict";

var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require("path");
var webpack = require("webpack");
var style_loader = require("style-loader");

var app_srcs = path.resolve(__dirname, "src/app/purs");
var app_entry = path.join(app_srcs, "App.purs");
var node_modules = path.resolve(__dirname, "node_modules");
var bower_components = path.resolve(__dirname, "bower_components");

var site_output = path.resolve(__dirname, "site");

var purs_build = path.resolve(__dirname, "psc_build");

module.exports = {
    entry: {
        "app": app_entry
    },

    output: {
        path: site_output,
        pathinfo: true,
        library: "[name]",
        libraryTarget: "var",
        filename: "assets/js/[name].js"
    },

    module: {
        loaders: [
            {
                test: /\.purs$/,
                loader: "purs-loader",
                query: {
                    src: [
                        "bower_components/purescript-*/src/**/*.purs",
                        path.join(app_srcs, "**/*.purs")
                    ],
                    bundle: false,
                    psc: "psc",
                    pscArgs: {
                        sourceMaps: true
                    },
                    pscIde: true,
                    output: purs_build
                }
            },

            {
                test: /\.scss$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            },

            {
                test: /\.js$/,
                loader: "source-map-loader"
            },

            {
                test: /\.js/,
                include: [
                    path.join(node_modules, "foundation-sites/js")
                ],
                loader: 'babel-loader',
                query: {
                    presets: ['es2015'],
                    compact: true
                }
            },

            {
                test: /(foundation.*\.js)/,
                loader: "imports?$=jquery"
            }
        ]
    },

    sassLoader: {
        includePaths: [
            path.join(node_modules, "/font-awesome/scss"),
            path.join(node_modules, "/foundation-sites/scss"),
            path.join(node_modules, "/motion-ui/src"),
            app_srcs
        ]
    },

    resolve: {
        modulesDirectories: ["node_modules", "bower_components", "src/app/scss"],
        extensions: [ "", ".purs", ".js", ".scss", ".css"]
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),

        new CopyWebpackPlugin([
            {
                context: "node_modules/font-awesome/fonts/",
                from: "*",
                to: "assets/fonts",
                toType: "dir"
            }
        ])
    ]
};
